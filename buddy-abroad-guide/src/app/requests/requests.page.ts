import { Component } from '@angular/core';
import { Router } from "@angular/router";
import { AuthService } from '../auth/auth.service';
import { AlertController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-requests',
  templateUrl: 'requests.page.html',
  styleUrls: ['requests.page.scss']
})
export class RequestsPage {

  auth: any;
  constructor(private toastCtrl: ToastController, private alertCtrl: AlertController, private router : Router, private authentication : AuthService) {
    this.checkAuth(); //Verifica se o guia turistico tem sessão iniciada aquando da construção da página
  }

  //Método para redireccionar para a página de login
  openLoginPage(){
    this.router.navigateByUrl("/auth");
  }

  //Método para verificar se o guia turistico tem sessão iniciada
  checkAuth(){
    this.authentication.isLoggedIn().subscribe((val: any) => {
      this.auth = val;
    });
  }

  //Método para exibição de popup para definição de data e hora do serviço aceite
  async openAcceptDialog() {
    const alert = await this.alertCtrl.create({
      header: 'Schedule trip',
      subHeader: 'Tourist: Christine',
      message: 'Set the day and time',
      inputs: [
        {
          name: 'date',
          label: 'Date',
          type: 'date'
        },
        {
          name: 'time',
          label: 'Time',
          type: 'time'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Confirm',
          handler: () => {
            let toast = this.toastCtrl.create({
              message: "Trip scheduled",
              duration: 1000
            }).then((toastData)=>{
              toastData.present();
            });
          }
        }
      ]
    });

    await alert.present();
  }

  //Método para exibição de popup para especificação do motivo de recusa
  async openRefuseDialog() {
    const alert = await this.alertCtrl.create({
      header: 'Refuse request',
      subHeader: 'Tourist: Christine',
      message: 'Specify the reason',
      inputs: [
        {
          name: 'msg',
          label: 'Message',
          type: 'text',
          placeholder: 'Reason'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Confirm',
          handler: () => {
            let toast = this.toastCtrl.create({
              message: "Message sent",
              duration: 1000
            }).then((toastData)=>{
              toastData.present();
            });
          }
        }
      ]
    });

    await alert.present();
  }

  //Método para exibição de popup para envio de mensagem
  async openMessageAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Send message',
      subHeader: 'To: Christine',
      inputs: [
        {
          name: 'msg',
          label: 'Message',
          type: 'text',
          placeholder: 'Your message'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Send',
          handler: () => {
            let toast = this.toastCtrl.create({
              message: "Message sent",
              duration: 1000
            }).then((toastData)=>{
              toastData.present();
            });
          }
        }
      ]
    });

    await alert.present();
  }

  routeImgSlideOpts = {
    initialSlide: 0,
    slidesPerView: 1,
    speed: 500,
    loop: true,
    autoplay: true,
    allowTouchMove: false,
    nested: true
  };
}
