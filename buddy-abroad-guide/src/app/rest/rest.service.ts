import { Injectable } from '@angular/core';
import { HttpClient } from  '@angular/common/http';
import { Observable } from  'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RestService {
  SERVER_ADDRESS: string = 'http://localhost:3000';
  constructor(private  httpClient : HttpClient) { }

  // Sending a GET request to /poitypes

  public getPOITypes(): Observable<Object> {
    return this.httpClient.get(this.SERVER_ADDRESS + '/poitypes');
  }
  
  public getLanguages(): Observable<Object> {
    return this.httpClient.get(this.SERVER_ADDRESS + '/languages');
  }

  public getGuideByEmail(email: JSON): Observable<Object> {
    return this.httpClient.post(this.SERVER_ADDRESS + '/guideexist', email);
  }

  public getGuideInfo(email: JSON): Observable<Object> {
    return this.httpClient.post(this.SERVER_ADDRESS + '/guideinfo', email);
  }
}