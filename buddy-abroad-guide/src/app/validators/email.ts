import {FormControl} from '@angular/forms';
import { RestService } from '../rest/rest.service';
import { Injectable } from '@angular/core';
 
@Injectable()
export class EmailValidator {

  constructor(private restService:RestService) {}

  checkEmail(control: FormControl): any {
     this.restService.getGuideByEmail(JSON.parse('{"email":"'+control.value+'"}')).subscribe((value : any) => {
        //console.log(value.user);
        if(value.user){
            control.setErrors({"email_exists": true});
        }
    });
    return Promise.resolve(null);
  }
}