import { CalendarComponent } from 'ionic2-calendar/calendar';
import { Component, ViewChild, OnInit, Inject, LOCALE_ID } from '@angular/core';
import { AlertController, ToastController } from '@ionic/angular';
import { formatDate } from '@angular/common';
import { Router } from "@angular/router";
import { AuthService } from '../auth/auth.service';


@Component({
  selector: 'app-bookings',
  templateUrl: 'bookings.page.html',
  styleUrls: ['bookings.page.scss']
})
export class BookingsPage implements OnInit {
  event = {
    title: '',
    touristName: '',
    startTime: '',
    endTime: '',
    allDay: false
  };
 
  minDate = new Date().toISOString();
 
  eventSource = [{
      title: "Photograph Lisbon - Streets & Culture",
      touristName: "Oliver",
      startTime:  new Date("2019-06-12T16:00:00.000Z"),
      endTime: new Date("2019-06-12T18:00:00.000Z"),
      allDay: false
    },
    {
      title: "Photograph Lisbon - Streets & Culture",
      touristName: "Fabio",
      startTime:  new Date("2019-07-22T15:00:00.000Z"),
      endTime: new Date("2019-07-22T17:00:00.000Z"),
      allDay: false
    }];
  viewTitle;
 
  calendar = {
    mode: 'month',
    currentDate: new Date()
  };
  
  
  @ViewChild(CalendarComponent) myCal: CalendarComponent;
  auth: any;
  futureOrPast : any;
  constructor(private toastCtrl: ToastController, private alertCtrl: AlertController, @Inject(LOCALE_ID) private locale: string, private authentication: AuthService, private router: Router) { 
    this.checkAuth(); //Verifica se o guia turistico tem sessão iniciada aquando da construção da página
    this.futureOrPast = "future"; //Define o separador "future" como default
  }
 
  ngOnInit() {
    this.resetEvent();
  }
 
  resetEvent() {
    this.event = {
      title: '',
      touristName: '',
      startTime: new Date().toISOString(),
      endTime: new Date().toISOString(),
      allDay: false
    };
  }
 
   
  //Método para modificar o corrente mês/semana/dia exibido
  next() {
    var swiper = document.querySelector('.swiper-container')['swiper'];
    swiper.slideNext();
  }
 
  back() {
    var swiper = document.querySelector('.swiper-container')['swiper'];
    swiper.slidePrev();
  }
  //-------------------------
  
  //Método para alternar a vista do calendário entre mês/semana/dia
  changeMode(mode) {
    this.calendar.mode = mode;
  }
 
  //Método para mudar o foco do calenário para a data actual
  today() {
    this.calendar.currentDate = new Date();
  }
 
  // Selected date reange and hence title changed
  onViewTitleChanged(title) {
    this.viewTitle = title;
  }
  
  //Método para exibição de informação do evento seleccionado
  async onEventSelected(event) {
    // Use Angular date pipe for conversion
    let start = formatDate(event.startTime, 'medium', this.locale);
    let end = formatDate(event.endTime, 'medium', this.locale);
  
    const alert = await this.alertCtrl.create({
      header: event.title,
      subHeader: event.touristName,
      message: 'From: ' + start + '<br><br>To: ' + end,
      buttons: ['OK']
    });
    alert.present();
  }
  
  onTimeSelected(ev) {
    let selected = new Date(ev.selectedTime);
    this.event.startTime = selected.toISOString();
    selected.setHours(selected.getHours() + 1);
    this.event.endTime = (selected.toISOString());
  }

  //Método para redireccionar para a página de login
  openLoginPage(){
    this.router.navigateByUrl("/auth");
   }

  //Método para verificar se o guia turistico tem sessão iniciada
  checkAuth(){
    this.authentication.isLoggedIn().subscribe((val: any) => {
      this.auth = val;
    });
  }
  
  //Método para alternar para o separador clicado
  segmentChanged(ev: any) {
    this.futureOrPast = ev.detail.value;
    //console.log(this.futureOrPast);
  }

  //Método para exibição de popup para envio de mensagem
  async openMessageAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Send message',
      subHeader: 'To: Fabio',
      inputs: [
        {
          name: 'msg',
          label: 'Message',
          type: 'text',
          placeholder: 'Your message'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Send',
          handler: () => {
            let toast = this.toastCtrl.create({
              message: "Message sent",
              duration: 1000
            }).then((toastData)=>{
              toastData.present();
            });
          }
        }
      ]
    });

    await alert.present();
  }

  //Método para exibição de popup para envio de rating
  async openRateAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Rating Oliver',
      subHeader: "On: Photograph Lisbon - Streets & Culture",
      message: 'How do you evaluate the tourist',
      inputs: [
        {
          name: 'msg',
          label: 'Comment',
          type: 'text',
          placeholder: 'Your comment'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Confirm',
          handler: () => {
            let toast = this.toastCtrl.create({
              message: "Rating sucessful",
              duration: 1000
            }).then((toastData)=>{
              toastData.present();
            });
          }
        }
      ]
    });
 
    await alert.present();
  }

  routeImgSlideOpts = {
    initialSlide: 0,
    slidesPerView: 1,
    speed: 500,
    loop: true,
    autoplay: true,
    allowTouchMove: false,
    nested: true
  };
}
