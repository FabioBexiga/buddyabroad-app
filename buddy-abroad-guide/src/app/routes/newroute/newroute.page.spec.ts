import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewroutePage } from './newroute.page';

describe('NewroutePage', () => {
  let component: NewroutePage;
  let fixture: ComponentFixture<NewroutePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewroutePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewroutePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
