import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-newroute',
  templateUrl: './newroute.page.html',
  styleUrls: ['./newroute.page.scss'],
})
export class NewroutePage implements OnInit {
  newRouteForm: FormGroup;
  constructor(public formBuilder: FormBuilder) { 
    this.newRouteForm = formBuilder.group({
      routeTitle: ['', [Validators.required, Validators.minLength(2), Validators.pattern('^[a-zA-Z]+$')]],
      routeDesc: ['', [Validators.required, Validators.minLength(2), Validators.pattern('^[a-zA-Z]+$')]],
      routelength: ['', [Validators.required, Validators.min(2), Validators.max(8), Validators.pattern('[0-9]*')]],
      maxGroupSize: ['', [Validators.required, Validators.min(1), Validators.max(10), Validators.pattern('[0-9]*')]],
      routePics: [''],
      routePOIs: ['', Validators.required]
      });
  }

  ngOnInit() {
  }

}
