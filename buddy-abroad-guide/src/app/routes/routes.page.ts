import { Component } from '@angular/core';
import { Router } from "@angular/router";
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-routes',
  templateUrl: 'routes.page.html',
  styleUrls: ['routes.page.scss']
})
export class RoutesPage {

  auth: any;
  constructor(private router : Router, private authentication : AuthService) {
    this.checkAuth(); //Verifica se o guia turistico tem sessão iniciada aquando da construção da página
  }

  //Método para redireccionar para a página de login
  openLoginPage(){
    this.router.navigateByUrl("/auth");
  }

  //Método para redireccionar para a página de criação de uma nova rota
  openNewRoutePage(){
    this.router.navigateByUrl("/newroute");
  }

  //Método para verificar se o guia turistico tem sessão iniciada
  checkAuth(){
    this.authentication.isLoggedIn().subscribe((val: any) => {
      this.auth = val;
    });
  }

  slideOpts = {
    initialSlide: 0,
    slidesPerView: 1,
    speed: 500,
    loop: true,
    autoplay: true,
    nested: true
  };
}
