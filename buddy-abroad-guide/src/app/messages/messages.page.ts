import { Component } from '@angular/core';
import { Router } from "@angular/router";
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-messages',
  templateUrl: 'messages.page.html',
  styleUrls: ['messages.page.scss']
})
export class MessagesPage {
  auth: any;
  constructor(private authentication: AuthService, private router: Router) {
    this.checkAuth(); //Verifica se o guia turistico tem sessão iniciada aquando da construção da página
  }

  //Método para redireccionar para a página de login
  openLoginPage(){
    this.router.navigateByUrl("/auth");
   }

  //Método para verificar se o guia turistico tem sessão iniciada
  checkAuth(){
    this.authentication.isLoggedIn().subscribe((val: any) => {
      this.auth = val;
    });
  }
 
  //Método para redireccionar para a página de detalhe da conversa
  openMessageDetail(){
    this.router.navigateByUrl('/messagedetail');
  }
}
