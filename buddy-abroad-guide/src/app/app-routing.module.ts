import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: 'auth', loadChildren: './auth/login-tabs/login-tabs.module#LoginTabsPageModule' },
  { path: 'tabs/account/personalinfo', loadChildren: './account/personalinfo/personalinfo.module#PersonalinfoPageModule' },
  { path: 'messagedetail', loadChildren: './messages/messagedetail/messagedetail.module#MessagedetailPageModule' },
  { path: 'newroute', loadChildren: './routes/newroute/newroute.module#NewroutePageModule' },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
