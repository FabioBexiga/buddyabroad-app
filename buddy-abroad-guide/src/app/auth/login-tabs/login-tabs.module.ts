import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { LoginTabsPageRoutingModule } from './login-tabs.router.module';

import { LoginTabsPage } from './login-tabs.page';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    LoginTabsPageRoutingModule
  ],
  declarations: [LoginTabsPage]
})
export class LoginTabsPageModule {}
