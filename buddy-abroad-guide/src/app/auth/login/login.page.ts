import { Component, OnInit } from '@angular/core';
import { Router } from  "@angular/router";
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: 'login.page.html',
  styleUrls: ['login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
  }
 
  //Método para utilização do método "login" do serviço de autentificação
  //enviado os dados inseridos no formulário
  login(form){
    this.authService.login(form.value).subscribe((res)=>{
      this.router.navigateByUrl('/');
    });
  }
}
