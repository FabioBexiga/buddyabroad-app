export interface AuthResponse {
    user: {
        guideId: number;
        firstName: string;
        lastName: string;
        email: string;
    },
    access_token: string,
    expires_in: number

}