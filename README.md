# BuddyAbroad app

Protótipo de aplicaçôes para turista e guia turistico utilizando a framework Ionic

---

## Setup

1. Instalar a versão 10.15.3 LTS do Node.js [Node.js x64](https://nodejs.org/download/release/v10.15.3/node-v10.15.3-x64.msi)
2. Abrir linha de comandos / powershell na raiz da pasta e executar os seguintes comandos:
    - "npm install -g ionic" para instalação da framework Ionic
3. Para execução da aplicação de guia turistico abrir linha de comandos / powershell na raiz da pasta "buddy-abroad-guide" e executar:
    - "npm install" para instalação das dependências
    - "ionic serve" para executar a aplicação no browser
    - Para terminar a aplicação efectuar ctrl+c
4. Para execução da aplicação de turista abrir linha de comandos / powershell na raiz da pasta "buddy-abroad-tourist" e executar:
    - "npm install" para instalação das dependências
    - "ionic serve" para executar a aplicação no browser
    - Para terminar a aplicação efectuar ctrl+c