import { Injectable } from '@angular/core';
import { HttpClient } from  '@angular/common/http';
import { Observable } from  'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RestService {
  SERVER_ADDRESS: string = 'http://localhost:3000';
  constructor(private  httpClient : HttpClient) { }

  //Método para obter os tipos de POI disponiveis através dum pedido GET para o método "poitypes"
  //do RESTful API Server
  public getPOITypes(): Observable<Object> {
    return this.httpClient.get(this.SERVER_ADDRESS + '/poitypes');
  }
  
  //Método para obter as linguas disponiveis através dum pedido GET para o método "languages"
  //do RESTful API Server
  public getLanguages(): Observable<Object> {
    return this.httpClient.get(this.SERVER_ADDRESS + '/languages');
  }

  //Método para verificar se o turista existe através dum pedido POST para o método "touristexist"
  //do RESTful API Server
  public getTouristByEmail(email: JSON): Observable<Object> {
    return this.httpClient.post(this.SERVER_ADDRESS + '/touristexist', email);
  }

  //Método para obter a informação do turista através dum pedido PSOT para o método "touristinfo"
  //do RESTful API Server
  public getTouristInfo(email: JSON): Observable<Object> {
    return this.httpClient.post(this.SERVER_ADDRESS + '/touristinfo', email);
  }
}