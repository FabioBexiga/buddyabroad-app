import { Component } from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'app-explore',
  templateUrl: 'explore.page.html',
  styleUrls: ['explore.page.scss']
})
export class ExplorePage {

  constructor(private router: Router) {}
  
  slideOpts = {
    initialSlide: 0,
    slidesPerView: 1,
    speed: 500,
    loop: true,
    autoplay: true,
    nested: true
  };

  //Método para redireccionar para a página de detalhe da cidade
  openCityPage(city) {
    this.router.navigateByUrl('/city')
  }

  //Método para redireccionar para a página de detalhe da rota
  openRoutePage(){
    this.router.navigateByUrl('/route')
  }
}
