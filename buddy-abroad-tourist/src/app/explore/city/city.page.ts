import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'app-city',
  templateUrl: './city.page.html',
  styleUrls: ['./city.page.scss'],
})
export class CityPage implements OnInit {
  city: string = "Lisbon";
  constructor(private router: Router) { }

  ngOnInit() {
  }

  slideOpts = {
    initialSlide: 0,
    slidesPerView: 1,
    speed: 500,
    loop: true,
    autoplay: true
  };
  
  //Método para redireccionar para a página de detalhe da rota
  openRoutePage(){
    this.router.navigateByUrl('/route')
  }
}
