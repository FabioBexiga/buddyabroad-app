export interface AuthResponse {
    user: {
        touristId: number;
        firstName: string;
        lastName: string;
        email: string;
    },
    access_token: string,
    expires_in: number

}