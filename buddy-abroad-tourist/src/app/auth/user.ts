export interface User {
    touristId: number;
    firstName: string;
    lastName: string;
    dateOfBirth: Date;
    mobile: number;
    email: string;
    password: string;
}