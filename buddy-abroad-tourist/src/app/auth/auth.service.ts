import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { Observable, BehaviorSubject } from 'rxjs';

import { Storage } from '@ionic/storage';
import { User } from './user';
import { AuthResponse } from './auth-response';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  AUTH_SERVER_ADDRESS: string = 'http://localhost:3000'; //Endereço do RESTful API Server
  authSubject = new BehaviorSubject(false);

  constructor(private httpClient: HttpClient, private storage: Storage, private toastCtrl: ToastController) { }

  //Método para efectuar o registo do turista enviando os dados recebidos para o RESTful API Server
  //através dum pedido POST para o método "registertourist"
  register(user: User): Observable<AuthResponse> {
    //console.log(user);
    return this.httpClient.post<AuthResponse>(this.AUTH_SERVER_ADDRESS+'/registertourist', user).pipe(
      tap(async (res: AuthResponse ) => {
        //Caso a resposta seja positiva armazena alguns dados localmente e inicia sessão
        if (res.user) {
          await this.storage.set("firstName", res.user.firstName);
          await this.storage.set("email", res.user.email);
          await this.storage.set("ACCESS_TOKEN", res.access_token);
          await this.storage.set("EXPIRES_IN", res.expires_in);
          this.authSubject.next(true);
        }
      })

    );
  }

  //Método para efectuar o login do turista enviando os dados recebidos para o RESTful API Server
  //através dum pedido POST para o método "logintourist"
  login(user: User): Observable<AuthResponse> {
    return this.httpClient.post(this.AUTH_SERVER_ADDRESS+'/logintourist', user).pipe(
      tap(async (res: AuthResponse) => {
        //Caso a resposta seja positiva armazena alguns dados localmente e inicia sessão
        if (res.user) {
          await this.storage.set("firstName", res.user.firstName);
          await this.storage.set("email", res.user.email);
          await this.storage.set("ACCESS_TOKEN", res.access_token);
          await this.storage.set("EXPIRES_IN", res.expires_in);
          this.authSubject.next(true);
          this.showToastMessage("Login sucessful!");
        }
      },
      err => {
          this.showToastMessage(err.error);
      })      
    );
  }

  //Método para terminar a sessão e limpar o dados armazenados localmente
  async logout() {
    await this.storage.remove("firstName");
    await this.storage.remove("email");
    await this.storage.remove("ACCESS_TOKEN");
    await this.storage.remove("EXPIRES_IN");
    this.authSubject.next(false);
  }

  //Método para verificar se o turista tem sessão iniciada
  isLoggedIn() {
    return this.authSubject.asObservable();
  }

  //Método para obter a informação do turista armazenada localmente
  getLoggedUserData() {
    return Promise.all([this.storage.get("firstName"), this.storage.get("email")]).then(values => {
      return values;
    });
  }

  //Método para exibição de mensagens ao turista
  showToastMessage(text: string){
    let toast = this.toastCtrl.create({
      message: text,
      duration: 1500
    }).then((toastData)=>{
      toastData.present();
    });
  }
}
