import { Component, OnInit } from '@angular/core';
import { Router } from  "@angular/router";
import { AuthService } from '../auth.service';
import { RestService } from '../../rest/rest.service';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { EmailValidator } from '../../validators/email';

@Component({
  selector: 'app-register',
  templateUrl: 'register.page.html',
  styleUrls: ['register.page.scss'],
})
export class RegisterPage implements OnInit {
  availablePOITypes: any; //Variavel para guardar os tipos de POI disponives
  availableLanguages : any; //Variavel para guardar as linguas disponives
  registrationForm: FormGroup;
  constructor(private emailValidator:EmailValidator, private authService: AuthService, private router: Router, private restService: RestService, public formBuilder: FormBuilder) { 
    //Validação dos campos do formulário
    this.registrationForm = formBuilder.group({
      firstName: ['', [Validators.required, Validators.minLength(2), Validators.pattern('^[a-zA-Z]+$')]],
      lastName: ['', [Validators.required, Validators.minLength(2), Validators.pattern('^[a-zA-Z]+$')]],
      dateOfBirth: ['', Validators.required],
      mobile: ['', [Validators.required, Validators.minLength(9), Validators.maxLength(20), Validators.pattern('[0-9]*')]],
      email: ['', [Validators.required, Validators.email, Validators.minLength(4)], emailValidator.checkEmail.bind(this.emailValidator)],
      password: ['', [Validators.required, Validators.minLength(4)]],
      languages: ['', Validators.required],
      interests: ['', Validators.required]
      });
  }

  ngOnInit() {
    //Obtem os tipos de POI disponiveis utilizando o método do serviço 'rest' após a construção da página
    this.restService.getPOITypes().subscribe((values : any) => {
      this.availablePOITypes = values;
    });

    //Obtem as linguas disponiveis utilizando o método do serviço 'rest' após a construção da página
    this.restService.getLanguages().subscribe((values : any) => {
      this.availableLanguages = values;
    });
  }

  //Método para utilização do método "register" do serviço de autentificação
  //enviado os dados inseridos no formulário
  register(form) {
    //console.log(form);
    this.authService.register(form.value).subscribe((res) => {
      this.router.navigateByUrl('/');
    });
  }

}
