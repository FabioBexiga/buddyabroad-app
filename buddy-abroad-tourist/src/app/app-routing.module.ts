import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: 'auth', loadChildren: './auth/login-tabs/login-tabs.module#LoginTabsPageModule' },
  { path: 'tabs/account/personalinfo', loadChildren: './account/personalinfo/personalinfo.module#PersonalinfoPageModule' },
  { path: 'city', loadChildren: './explore/city/city.module#CityPageModule' },
  { path: 'newbooking', loadChildren: './bookings/newbooking/newbooking.module#NewbookingPageModule' },
  { path: 'messagedetail', loadChildren: './messages/messagedetail/messagedetail.module#MessagedetailPageModule' },
  { path: 'route', loadChildren: './explore/route/route.module#RoutePageModule' }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
