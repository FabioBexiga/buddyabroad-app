import { Component, OnInit } from '@angular/core';
import { RestService } from '../../rest/rest.service';
import { AuthService } from '../../auth/auth.service';
import { Router } from "@angular/router";
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-personalinfo',
  templateUrl: './personalinfo.page.html',
  styleUrls: ['./personalinfo.page.scss'],
})
export class PersonalinfoPage implements OnInit {
  auth: any; //Variavel usada para controlar o que mostrar consoante o estado de autentificação
  email: string; //Variavel para guardar o email do turista caso tenha sessão iniciada
  touristInfo: any; //Variavel para guardar a informação pessoal do turista
  touristLanguages : any = []; //Variavel para guardar as linguas do turista
  touristInterests : any = [];  //Variavel para guardar os interesses do turista
  availablePOITypes: any; //Variavel para guardar os tipos de POI disponives
  availableLanguages : any; //Variavel para guardar as linguas disponiveis
  personalInfoForm: FormGroup;
  constructor(private restService: RestService, public formBuilder: FormBuilder, private authentication: AuthService, private router: Router) { 
    this.checkAuth(); //Verifica se o turista tem sessão iniciada aquando da construção da página
    //Validação dos campos do formulário
    this.personalInfoForm = formBuilder.group({
      firstName: ['', [Validators.required, Validators.minLength(2), Validators.pattern('^[a-zA-Z]+$')]],
      lastName: ['', [Validators.required, Validators.minLength(2), Validators.pattern('^[a-zA-Z]+$')]],
      dateOfBirth: ['', Validators.required],
      email: [''],
      mobile: ['', [Validators.required, Validators.minLength(9), Validators.maxLength(20), Validators.pattern('[0-9]*')]],
      password: ['', [Validators.required, Validators.minLength(4)]],
      languages: ['', Validators.required],
      interests: ['', Validators.required]
      });
  }

  ngOnInit() {
    //Obtem os tipos de POI disponiveis utilizando o método do serviço 'rest' após a construção da página
    this.restService.getPOITypes().subscribe((values : any) => {
      this.availablePOITypes = values;
    });

    //Obtem as linguas disponiveis utilizando o método do serviço 'rest' após a construção da página
    this.restService.getLanguages().subscribe((values : any) => {
      this.availableLanguages = values;
    });
  }

  //Método para verificar se o turista tem sessão iniciada e obter os seus dados
  //utilizando o método "getTouristInfo" do serviço 'rest'
  checkAuth(){
    this.authentication.isLoggedIn().subscribe((val: any) => {
      this.auth = val;
      if(val) {
        this.authentication.getLoggedUserData().then((values:any) => {
          this.email = values[1];
          this.restService.getTouristInfo(JSON.parse('{"email":"'+this.email+'"}')).subscribe((info : any) => {
            this.touristInfo = info.tourist;
            //console.log(info);
            for(let idx in info.languages)
              this.touristLanguages.push(info.languages[idx].languageId);
            for(let idx in info.interests)
              this.touristInterests.push(info.interests[idx].poiTypeId);
            
            //console.log(this.touristInterests);
          });
        });
      }  
      else
        this.router.navigateByUrl('/tabs/account');
    });
  }
}
