import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Router } from "@angular/router";
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-account',
  templateUrl: 'account.page.html',
  styleUrls: ['account.page.scss'],
})
export class AccountPage {
  auth: any; //Variavel usada para controlar o que mostrar consoante o estado de autentificação
  firstName: string;
  email: string;
  constructor(private authentication: AuthService, private router: Router) {
    this.checkAuth(); //Verifica se o turista tem sessão iniciada aquando da construção da tab account
  }

  //Método para redireccionar para a página de login
  openLoginPage(){
   this.router.navigateByUrl("/auth");
  }

  //Método para redireccionar para a página de registo
  openRegisterPage(){
    this.router.navigateByUrl("/auth/register");
  }

  //Método para terminar sessão e redireccionar para a tab account
  clearAuth(){
    this.authentication.logout();
    this.router.navigateByUrl("/tabs/account");
  }

  //Método para verificar se o turista tem sessão iniciada e obter os seus dados armazenados
  checkAuth(){
    this.authentication.isLoggedIn().subscribe((val: any) => {
      this.auth = val;
      if(val) {
        this.authentication.getLoggedUserData().then((values:any) => {
          this.firstName = values[0];
          this.email = values[1];
        });
      }  
    });
  }

  //Método para redireccionar para a página personalinfo
  openPersonalInfoPage(){
    this.router.navigateByUrl("/tabs/account/personalinfo");
  }

}
