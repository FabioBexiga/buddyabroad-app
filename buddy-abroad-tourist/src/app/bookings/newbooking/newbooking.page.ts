import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'app-newbooking',
  templateUrl: './newbooking.page.html',
  styleUrls: ['./newbooking.page.scss'],
})
export class NewbookingPage implements OnInit {
  show: boolean = false;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  showRoutes(){
    this.show = true;
  }

  //Método para redireccionar para a página de detalhe da rota
  openRoutePage(){
    this.router.navigateByUrl('/route')
  }
  routeImgSlideOpts = {
    initialSlide: 0,
    slidesPerView: 1,
    speed: 500,
    loop: true,
    autoplay: true,
    allowTouchMove: false
  };
}
