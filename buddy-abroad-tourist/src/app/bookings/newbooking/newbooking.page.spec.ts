import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewbookingPage } from './newbooking.page';

describe('NewbookingPage', () => {
  let component: NewbookingPage;
  let fixture: ComponentFixture<NewbookingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewbookingPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewbookingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
